package analizador;

import java_cup.runtime.Symbol;
%%
%class LexerCup
%type java_cup.runtime.Symbol
%cup
%full
%line
%char
%{
    private Symbol symbol(int type, Object value){
        return new Symbol(type, yyline, yycolumn, value);
    }
    private Symbol symbol(int type){
        return new Symbol(type, yyline, yycolumn);
    }
%}

//LANGUAGE
L=[a-zA-Z_]+
D=[0-9]+
ESP=[ ,\t,\r]+
SIMB = [#!$%&?¡_]+
L_A = [ñÑáéíóúÁÉÍÓÚ]
%%

{ESP} {/*Ignore*/}  //Espacio  
( "//"(.)* ) {/*Ignore*/}   //Comentario
( "\n" ) {return Linea;}    //Linea

"<<EOF>>" {return new Symbol(sym.OPERADOR, yyline, yycolumn, yytext());}
//COMILLAS
"\"" {return new Symbol(sym.COMILLAS, yyline, yycolumn, yytext());}
//OPERADOR COMA
"," {return new Symbol(sym.OP_COMA, yyline, yycolumn, yytext());}
//OPERADOR PUNTO Y COMA
";" {return new Symbol(sym.OP_PUNTO_Y_COMA, yyline, yycolumn, yytext());}
//OPERADOR IGUAL, DE ASIGNACION
"=" {return new Symbol(sym.OP_IGUAL, yyline, yycolumn, yytext());}
//PARENTESIS
"(" {return new Symbol(sym.PARENTESIS_A, yyline, yycolumn, yytext());}
")" {return new Symbol(sym.PARENTESIS_C, yyline, yycolumn, yytext());}
"[" {return new Symbol(sym.CORCHETE_A, yyline, yycolumn, yytext());}
"]" {return new Symbol(sym.CORCHETE_C, yyline, yycolumn, yytext());}
"{" {return new Symbol(sym.LLAVE_A, yyline, yycolumn, yytext());}
"}" {return new Symbol(sym.LLAVE_C, yyline, yycolumn, yytext());}
//OPERADORES MAT
"+" {return new Symbol(sym.OP_SUMA, yyline, yycolumn, yytext());}
"-" {return new Symbol(sym.OP_RESTA, yyline, yycolumn, yytext());}
"*" {return new Symbol(sym.OP_MULTIPLICACION, yyline, yycolumn, yytext());}
"/" {return new Symbol(sym.OP_DIVISION, yyline, yycolumn, yytext());}
//OPERADORES INCREMENTO DECREMENTO
"++" {return new Symbol(sym.OP_INCREMENTO, yyline, yycolumn, yytext());}
"--" {return new Symbol(sym.OP_INCREMENTO, yyline, yycolumn, yytext());}
//OPERADORES RELACIONALES
">" {return new Symbol(sym.OP_RELACIONAL, yyline, yycolumn, yytext());}
">=" {return new Symbol(sym.OP_RELACIONAL, yyline, yycolumn, yytext());}
"<" {return new Symbol(sym.OP_RELACIONAL, yyline, yycolumn, yytext());}
"<=" {return new Symbol(sym.OP_RELACIONAL, yyline, yycolumn, yytext());}
"==" {return new Symbol(sym.OP_RELACIONAL, yyline, yycolumn, yytext());}
"!=" {return new Symbol(sym.OP_RELACIONAL, yyline, yycolumn, yytext());}
//TIPOS DE DATOS
"INT" {return new Symbol(sym.INT, yyline, yycolumn, yytext());}
"BOOLEAN" {return new Symbol(sym.BOOLEAN, yyline, yycolumn, yytext());}
"STRING" {return new Symbol(sym.STRING, yyline, yycolumn, yytext());}
//PALABRAS RESERVADAS
"FOR" {return new Symbol(sym.FOR, yyline, yycolumn, yytext());}
"IF" {return new Symbol(sym.IF, yyline, yycolumn, yytext());}
"ELSE" {return new Symbol(sym.ELSE, yyline, yycolumn, yytext());}
//SENTENCIAS
{return new Symbol(sym.OPERADOR, yyline, yychar, yytext());}
//IDENTIFICADORES
{L}(({L}|{D}){0, 70})? {return new Symbol(sym.IDENTIFICADOR, yyline, yycolumn, yytext());}
//VALORES
"({L}|{D}|{ESP})*+\" {lexeme=yytext(); line=yyline; return VALOR_STRING;}
("(-"{D}+")")|{D}+ {return new Symbol(sym.VALOR_NUMERO, yyline, yycolumn, yytext());} // INT
"True" {return new Symbol(sym.TRUE, yyline, yycolumn, yytext());}//BOOLEAN
"False" {return new Symbol(sym.FALSE, yyline, yycolumn, yytext());}//BOOLEAN
//ERRORES
            //IDENTIFICADOR MAYOR A 71 
{L}(({L}|{D}){71})({L}|{D})* {return new Symbol(sym.ERROR_IDENTIFICADOR, yyline, yycolumn, yytext());}
            //IDENTIFICADOR SOLO PUEDE EMPEZAR CON LETRAS, Y SIN ACENTOS
(({D}+)({L}|{L_A}))(({L}|{S}|{SIMB}|{L_A}))* {return new Symbol(sym.ERROR_IDENTIFICADOR, yyline, yycolumn, yytext());}
            //IDENTIFICADOR NO PUEDE CONTENER SIMBOLOS O ACENTOS
({L}|{L_A}|{SIMB})(({L}|{D}|{SIMB}|{L_A}))+ {return new Symbol(sym.ERROR_IDENTIFICADOR, yyline, yycolumn, yytext());}
            //SOLO SE ACEPTAN NUMEROS ENTEROS - NUMERO PUNTO NUMERO NO ES VÁLIDO           
{D}*+("."{D}*)+ {return new Symbol(sym.ERROR_VALOR, yyline, yycolumn, yytext());}
            //NUMERO PUNTO NUMERO PUNTO NUMERO ES MENOS VÁLIDO            
{D}+"."{D}+("."{D}*)+ {return new Symbol(sym.ERROR_VALOR, yyline, yycolumn, yytext());}
            //LOS NUMEROS NO PUEDEN CONTENER LETRAS
({D}+{L}+"."{D}+) | ({L}+{D}+"."{D}+) {return new Symbol(sym.ERROR_VALOR, yyline, yycolumn, yytext());}
            //LOS NUMEROS NO PUEDEN TERMINAR EN LETRAS
({D}+"."{D}+{L}+) | ({D}+"."{L}+{D}+) {return new Symbol(sym.ERROR_VALOR, yyline, yycolumn, yytext());}
            //LOS NUMEROS NO PUEDEN EMPEZAR CON EN LETRAS
({L}+"."{D}+{L}+) | ({L}+"."{L}+{D}+) {return new Symbol(sym.ERROR_VALOR, yyline, yycolumn, yytext());}
            //LOS NUMEROS NO PUEDEN TERMINAR EN PUNTO
({D}+{L}*".") {return new Symbol(sym.ERROR_VALOR, yyline, yycolumn, yytext());}
            //LOS NUMEROS NO PUEDEN LLEVAR COMAS
{D}+","{D}+ {return new Symbol(sym.ERROR_VALOR, yyline, yycolumn, yytext());}
            //ERRORES LETRAS
"#"{L}+ {return new Symbol(sym.ERROR_VALOR, yyline, yycolumn, yytext());}
'[^'] ~' {return new Symbol(sym.ERROR_VALOR, yyline, yycolumn, yytext());}
            //ERROR ANALISIS
. {return new Symbol(sym.error, yyline, yycolumn, yytext());}





