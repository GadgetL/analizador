package analizador;

import static codigo.Tokens.*;
%%
%class Lexer
%type Tokens
%{
    public String lexeme;
%}
//LANGUAGE
L=[a-zA-Z_]+
D=[0-9]+
ESP=[ ,\t,\r]+
SIMB = [#!$%&?¡_]+
L_A = [ñÑáéíóúÁÉÍÓÚ]
%%

{ESP} {/*Ignore*/}  //Espacio  
( "//"(.)* ) {/*Ignore*/}   //Comentario
( "\n" ) {return Linea;}    //Linea

"<<EOF>>" {return new Symbol(sym.OPERADOR, yyline, yycolumn, yytext());}
//COMILLAS
( "\"" ) {lexeme=yytext(); return COMILLAS;}
//OPERADOR COMA
( "," ) {lexeme=yytext(); return OP_COMA;}
//OPERADOR PUNTO Y COMA
( ";" ) {lexeme=yytext(); return OP_PUNTO_Y_COMA;}
//OPERADOR IGUAL, DE ASIGNACION
( "=" ) {lexeme=yytext(); return OP_IGUAL;}
//PARENTESIS
( "(" ) {lexeme=yytext(); return PARENTESIS_A;}
( ")" ) {lexeme=yytext(); return PARENTESIS_C;}
( "[" ) {lexeme=yytext(); return CORCHETE_A;}
( "]" ) {lexeme=yytext(); return CORCHETE_C;}
( "{" ) {lexeme=yytext(); return LLAVE_A;}
( "}" ) {lexeme=yytext(); return LLAVE_C;}
//OPERADORES MAT
( "+" ) {lexeme=yytext(); return OP_SUMA;}
( "-" ) {lexeme=yytext(); return OP_RESTA;}
( "*" ) {lexeme=yytext(); return OP_MULTIPLICACION;}
( "/" ) {lexeme=yytext(); return OP_DIVISION;}
//OPERADORES INCREMENTO DECREMENTO
( "++" ) {lexeme=yytext(); return OP_INCREMENTO;}
( "--" ) {lexeme=yytext(); return OP_INCREMENTO;} 
//OPERADORES RELACIONALES
( ">" ) {lexeme=yytext(); return OP_RELACIONAL;}
( ">=" ) {lexeme=yytext(); return OP_RELACIONAL;}
( "<" ) {lexeme=yytext(); return OP_RELACIONAL;}
( "<=" ) {lexeme=yytext(); return OP_RELACIONAL;}
( "==" ) {lexeme=yytext(); return OP_RELACIONAL;}
( "!=" ) {lexeme=yytext(); return OP_RELACIONAL;}
//TIPOS DE DATOS
( "INT" ) {lexeme=yytext(); return INT;}
( "BOOLEAN" ) {lexeme=yytext(); return BOOLEAN;}
( "STRING" ) {lexeme=yytext(); return STRING;}
//PALABRAS RESERVADAS
( "FOR" ) {lexeme=yytext(); return FOR;}
( "IF" ) {lexeme=yytext(); return IF;}
( "ELSE" ) {lexeme=yytext(); return ELSE;}
//SENTENCIAS
{lexeme=yytext(); return OPERADOR;}
//IDENTIFICADORES
{L}(({L}|{D}){0, 70})? {lexeme=yytext(); return IDENTIFICADOR;}
//VALORES
"({L}|{D}|{ESP})*+\" {lexeme=yytext(); return VALOR_STRING;}
("(-"{D}+")")|{D}+ {lexeme=yytext(); return VALOR_NUMERO;}
"True" {lexeme=yytext(); return TRUE;}
"False" {lexeme=yytext(); return FALSE;}
//ERRORES
            //IDENTIFICADOR MAYOR A 71 
{L}(({L}|{D}){71})({L}|{D})* {lexeme=yytext(); return ERROR_IDENTIFICADOR;}
            //IDENTIFICADOR SOLO PUEDE EMPEZAR CON LETRAS, Y SIN ACENTOS
(({D}+)({L}|{L_A}))(({L}|{S}|{SIMB}|{L_A}))* {lexeme=yytext(); return ERROR_IDENTIFICADOR;}
            //IDENTIFICADOR NO PUEDE CONTENER SIMBOLOS O ACENTOS
({L}|{L_A}|{SIMB})(({L}|{D}|{SIMB}|{L_A}))+ {lexeme=yytext(); return ERROR_IDENTIFICADOR;}
            //SOLO SE ACEPTAN NUMEROS ENTEROS - NUMERO PUNTO NUMERO NO ES VÁLIDO           
{D}*+("."{D}*)+ {lexeme=yytext(); return ERROR_VALOR;}
            //NUMERO PUNTO NUMERO PUNTO NUMERO ES MENOS VÁLIDO            
{D}+"."{D}+("."{D}*)+ {lexeme=yytext(); return ERROR_VALOR;}
            //LOS NUMEROS NO PUEDEN CONTENER LETRAS
({D}+{L}+"."{D}+) | ({L}+{D}+"."{D}+) {lexeme=yytext(); return ERROR_VALOR;}
            //LOS NUMEROS NO PUEDEN TERMINAR EN LETRAS
({D}+"."{D}+{L}+) | ({D}+"."{L}+{D}+) {lexeme=yytext(); return ERROR_VALOR;}
            //LOS NUMEROS NO PUEDEN EMPEZAR CON EN LETRAS
({L}+"."{D}+{L}+) | ({L}+"."{L}+{D}+) {lexeme=yytext(); return ERROR_VALOR;}
            //LOS NUMEROS NO PUEDEN TERMINAR EN PUNTO
({D}+{L}*".") {lexeme=yytext(); return ERROR_VALOR;}
            //LOS NUMEROS NO PUEDEN LLEVAR COMAS
{D}+","{D}+ {lexeme=yytext(); return ERROR_VALOR;}
            //ERRORES LETRAS
"#"{L}+ {lexeme=yytext(); return ERROR_VALOR;}
'[^'] ~' {lexeme=yytext(); return ERROR_VALOR;}
            //ERROR ANALISIS
. {return ERROR;}






