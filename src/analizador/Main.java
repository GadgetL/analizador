
package analizador;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) throws Exception, IOException {
        String lexerflex = "C:/Users/Laura/Desktop/LyA/Proyectos/Analizador/src/analizador/Lexer.flex"; 
        String lexercup = "C:/Users/Laura/Desktop/LyA/Proyectos/Analizador/src/analizador/LexerCup.flex";
        String[] sintasixcup = {"-parser", "Sintax", "C:/Users/Laura/Desktop/LyA/Proyectos/Analizador/src/analizador/Sintax.cup"};        
              
        File generar;
        generar = new File(lexerflex);
        JFlex.Main.generate(generar);        
        generar = new File(lexercup);
        JFlex.Main.generate(generar);
        java_cup.Main.main(sintasixcup);
        
        Path rutaSym = Paths.get("C:/Users/Laura/Desktop/LyA/Proyectos/Analizador/src/analizador/sym.java");
        if (Files.exists(rutaSym)) {
            Files.delete(rutaSym);
        }
        Files.move(
                Paths.get("C:/Users/Laura/Desktop/LyA/Proyectos/Analizador/src/analizador/sym.java"), 
                Paths.get("C:/Users/Laura/Desktop/LyA/Proyectos/Analizador/src/analizador/sym.java")
        );
               
        Path rutaSintaxis = Paths.get("C:/Users/Laura/Desktop/LyA/Proyectos/Analizador/src/analizador/Sintax.java");
        if (Files.exists(rutaSintaxis)) {
            Files.delete(rutaSintaxis);
        }
        Files.move(
                Paths.get("C:/Users/Laura/Desktop/LyA/Proyectos/Analizador/Sintax.java"), 
                Paths.get("C:/Users/Laura/Desktop/LyA/Proyectos/Analizador/src/analizador/Sintax.java")
        );
    }
}
